﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HostApp.DataModel;
using System.Collections.Generic;

namespace Tests
{
    /// <summary>
    /// Performs tests on actually started webservices.
    /// Uses different port than the HostApp (8081 instead of 8080) and different database (UsersTestDB instead of UsersDB).
    /// Both webservices are started before each test and stopped afterwards.
    /// </summary>
    [TestClass]
    public class MainTests : AbstractTestClass
    {
        const string remoteUri = "http://localhost:8081";

        private HostApp.WCF.UserInfoServiceWrapper infoService;
        private HostApp.WebApi.UserImportServiceWrapper importService;
        
        #region Tests

        /// <summary>
        /// Starts both services before test
        /// </summary>
        [TestInitialize]
        public void StartServices()
        {
            this.importService = new HostApp.WebApi.UserImportServiceWrapper(remoteUri);
            this.infoService = new HostApp.WCF.UserInfoServiceWrapper(remoteUri);
        }

        /// <summary>
        /// Stops both services after test
        /// </summary>
        [TestCleanup]
        public void StopServices()
        {
            this.importService.Dispose();
            this.infoService.Dispose();
        }

        /// <summary>
        /// Import a new unique user and the get info about it
        /// </summary>
        [TestMethod]
        public void UserImport_PositiveScenario()
        {
            try
            {
                var request = GenerateNewRequest();
                var importResult = CallUserImportService(request);

                Assert.IsTrue(importResult.StatusCode == System.Net.HttpStatusCode.OK);

                var infoResult = CallUserInfoService(request.UserId);

                Assert.AreEqual(request.UserId, infoResult.UserId);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Try to get info for non-existing user
        /// </summary>
        [TestMethod]
        public void UserInfo_UserNotFound()
        {
            Guid requestID = Guid.NewGuid();
            try
            {
                var result = CallUserInfoService(requestID);
            }
            catch (System.ServiceModel.FaultException<ServiceReference.UserNotFound> ex)
            {
                Assert.AreEqual("User not found", ex.Detail.ErrorDetails, "Unexpected error code: {0}", ex.Detail.ErrorDetails);
                Assert.AreEqual(requestID, ex.Detail.UserId, "Returned UserID ({0}) is different from requested ({1})", ex.Detail.UserId, requestID);
            }
        }

        /// <summary>
        /// Import a user, check that it's in DB (by getting info about it)
        /// Then modify request data (all except UserID) and repeat the Import and Info request. 
        /// Info request should return updated values.
        /// </summary>
        [TestMethod]
        public void UserUpdate_PositiveScenario()
        {
            try
            {
                var request = GenerateNewRequest();

                //first, import user to DB
                var importResult = CallUserImportService(request);
                Assert.IsTrue(importResult.StatusCode == System.Net.HttpStatusCode.OK);

                //check that user is successfully added
                var infoResult = CallUserInfoService(request.UserId);
                Assert.AreEqual(request.UserId, infoResult.UserId);

                //change user details
                request.CountryIsoCode = "EN";
                request.RequestId = Guid.NewGuid();
                request.DateModified = DateTime.Now.AddDays(4);
                request.AdvertisingOptIn = null;
                request.Locale = "de-DE";

                //update user to the DB
                var updateResult = CallUserImportService(request);
                Assert.IsTrue(updateResult.StatusCode == System.Net.HttpStatusCode.OK);

                //check that user data is updated
                var updateCheckResult = CallUserInfoService(request.UserId);
                Assert.AreEqual(request.CountryIsoCode, updateCheckResult.CountryIsoCode);
                Assert.AreEqual(request.DateModified.ToString(), updateCheckResult.DateModified.ToString(), ignoreCase: true);
                Assert.AreEqual(request.AdvertisingOptIn, updateCheckResult.AdvertisingOptIn);
                Assert.AreEqual(request.Locale, updateCheckResult.Locale);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Send some non-json data and check general error code.
        /// </summary>
        [TestMethod]
        public void UserImport_TestBadRequest()
        {
            string badRequest = "non-json data string";
            try
            {
                var importResult = CallUserImportService(badRequest);
                
                Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, importResult.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Check that Locale field's validation check catches various invalid values.
        /// </summary>
        [TestMethod]
        public void UserImport_TestBadLocales()
        {
            foreach (string locale in badLocales)
                this.CheckLocaleIsInvalid(locale);
        }

        /// <summary>
        /// Check that CountryIsoCode field's validation check catches various invalid values.
        /// </summary>
        [TestMethod]
        public void UserImport_TestBadCountryCodes()
        {
            foreach (string code in badCountryCodes)
                this.CheckCountryCodeIsInvalid(code);
        }

        /// <summary>
        /// Attempt to import user on a stopped webservice
        /// </summary>
        [TestMethod]
        public void UserImport_On_Stopped_Service()
        {
            try
            {
                //kill the service
                this.importService.Dispose();
                
                var result = CallUserImportService(this.GenerateNewRequest());

                Assert.AreEqual(System.Net.HttpStatusCode.NotFound, result.StatusCode);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Attempt to get info about a user on stopped service
        /// </summary>
        [TestMethod]
        public void UserInfo_On_Stopped_Service()
        {
            try
            {
                //kill the service
                this.infoService.Dispose();

                var result = CallUserInfoService(Guid.NewGuid());
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(System.ServiceModel.EndpointNotFoundException));
            }
        }

        #endregion

        #region Data validation test helpers
        
        /// <summary>
        /// Calls UserImport service with given data and uses <paramref name="checkerDelegate"/>
        /// to be sure that expected error has been thrown.
        /// </summary>
        /// <param name="request">Request data object with some 'bad' data</param>
        /// <param name="checkerDelegate">checks that error details are the ones that are expected for given 'bad' data</param>
        protected override bool CheckUserImportForDataValidation(SyncProfileRequest request, Func<SyncProfileRequest, string, bool> checkerDelegate)
        {
            try
            {
                var response = CallUserImportService(request);

                if (response.IsSuccessStatusCode)
                    return false;

                //get details
                var rt = response.Content.ReadAsStringAsync();
                rt.Wait();

                return checkerDelegate(request, rt.Result);
            }
            catch (Exception ex)
            {
                Assert.Fail("Unexpected error code: {0}", ex.Message);
                return false;
            }
        }

        #endregion

        #region Service calling methods

        /// <summary>
        /// Calls UserImport Service
        /// </summary>
        /// <param name="requestData">Usually, should be of type <typeparamref name="SyncProfileRequest"/>, but can be any type.</param>
        /// <param name="controllerUri">By default is set to "import" - the actual controller name, but can be any string</param>
        HttpResponseMessage CallUserImportService(object requestData, string controllerUri = "import.json")
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(remoteUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                Task<HttpResponseMessage> httpReqTask;
                if (requestData is SyncProfileRequest)
                {
                    httpReqTask = client.PostAsJsonAsync<SyncProfileRequest>(controllerUri, (SyncProfileRequest)requestData);
                }
                else
                {
                    httpReqTask = client.PostAsJsonAsync(controllerUri, requestData.ToString());
                }
                httpReqTask.Wait();
                HttpResponseMessage response = httpReqTask.Result;
                
                return response;
            }
        }

        /// <summary>
        /// Calls UserInfo Service
        /// </summary>
        private ServiceReference.UserInfo CallUserInfoService(Guid UserId)
        {
            using (var client = new ServiceReference.UserInfoProviderClient())
            {
                return client.GetUserInfo(UserId);
            }
        }

        #endregion
                
    }
}
