﻿using System;
using System.Data.Entity;
using HostApp.DataModel;

namespace Tests.MockedDB
{
    class TestUserContext : IUserContext
    {
        public TestUserContext()
        {
            this.SyncProfileRequests = new TestUserDbSet();
        }
        
        public DbSet<SyncProfileRequest> SyncProfileRequests { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void Dispose() { this.SyncProfileRequests = null; }
    }
}
