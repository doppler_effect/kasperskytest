﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HostApp.DataModel;
using System.Collections.Generic;

namespace Tests
{
    /// <summary>
    /// Performs tests directly on WebApi controller and WCF provider.
    /// Uses a mocked database context, no connections to real DB are created,
    /// and no webservices are actually started.
    /// </summary>
    [TestClass]
    public class MockedDBtests : AbstractTestClass
    {
        private MockedDB.TestUserContext mockedContext;
        private HostApp.WebApi.ImportController importController;
        private HostApp.WCF.UserInfoProvider infoProvider;

        [TestInitialize]
        public void Initialize()
        {
            this.mockedContext = new MockedDB.TestUserContext();
            this.importController = new HostApp.WebApi.ImportController(this.mockedContext);
            this.infoProvider = new HostApp.WCF.UserInfoProvider(this.mockedContext);
        }

        /// <summary>
        /// Import a new unique user and the get info about it
        /// </summary>              
        [TestMethod]
        public void MockedDB_UserImport_PositiveScenario()
        {
            try
            {
                SyncProfileRequest request = base.GenerateNewRequest();
                var importResult = this.importController.PostSyncProfileRequest(request);
                Assert.IsInstanceOfType(importResult, typeof(System.Web.Http.Results.OkResult));

                var infoResult = this.infoProvider.GetUserInfo(request.UserId);
                Assert.AreEqual(request.UserId, infoResult.UserId);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Try to get info for non-existing user
        /// </summary>
        [TestMethod]
        public void MockedDB_UserInfo_UserNotFound()
        {
            Guid requestID = Guid.NewGuid();
            try
            {
                var result = this.infoProvider.GetUserInfo(requestID);
            }
            catch (System.ServiceModel.FaultException<HostApp.WCF.UserNotFound> ex)
            {
                Assert.AreEqual("User not found", ex.Detail.ErrorDetails, "Unexpected error code: {0}", ex.Detail.ErrorDetails);
                Assert.AreEqual(requestID, ex.Detail.UserId, "Returned UserID ({0}) is different from requested ({1})", ex.Detail.UserId, requestID);
            }
        }

        /// <summary>
        /// Import a user, check that it's in DB (by getting info about it)
        /// Then modify request data (all except UserID) and repeat the Import and Info request. 
        /// Info request should return updated values.
        /// </summary>
        [TestMethod]
        public void MockedDB_UserUpdate_PositiveScenario()
        {
            try
            {
                SyncProfileRequest request = GenerateNewRequest();

                //first, import user to DB
                var importResult = this.importController.PostSyncProfileRequest(request);
                Assert.IsInstanceOfType(importResult, typeof(System.Web.Http.Results.OkResult));

                //check that user is successfully added
                var infoResult = this.infoProvider.GetUserInfo(request.UserId);
                Assert.AreEqual(request.UserId, infoResult.UserId);

                //change user details
                request.CountryIsoCode = "EN";
                request.RequestId = Guid.NewGuid();
                request.DateModified = DateTime.Now.AddDays(4);
                request.AdvertisingOptIn = null;
                request.Locale = "de-DE";

                //update user to the DB
                var updateResult = this.importController.PostSyncProfileRequest(request);
                Assert.IsInstanceOfType(importResult, typeof(System.Web.Http.Results.OkResult));

                //check that user data is updated
                var updateCheckResult = this.infoProvider.GetUserInfo(request.UserId);
                Assert.AreEqual(request.CountryIsoCode, updateCheckResult.CountryIsoCode);
                Assert.AreEqual(request.DateModified.ToString(), updateCheckResult.DateModified.ToString(), ignoreCase: true);
                Assert.AreEqual(request.AdvertisingOptIn, updateCheckResult.AdvertisingOptIn);
                Assert.AreEqual(request.Locale, updateCheckResult.Locale);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        /// <summary>
        /// Check that Locale field's validation check catches various invalid values.
        /// </summary>
        [TestMethod]
        public void MockedDB_UserImport_TestBadLocales()
        {
            foreach (string locale in badLocales)
                this.CheckLocaleIsInvalid(locale);
        }

        /// <summary>
        /// Check that CountryIsoCode field's validation check catches various invalid values.
        /// </summary>
        [TestMethod]
        public void MockedDB_UserImport_TestBadCountryCodes()
        {
            foreach (string code in badCountryCodes)
                this.CheckCountryCodeIsInvalid(code);
        }
        
        /// <summary>
        /// Imports given data and uses <paramref name="checkerDelegate"/>
        /// to be sure that expected error has been returned.
        /// </summary>
        /// <param name="request">Request data object with some 'bad' data</param>
        /// <param name="checkerDelegate">checks that error details are the ones that are expected for given 'bad' data</param>
        protected override bool CheckUserImportForDataValidation(SyncProfileRequest request, Func<SyncProfileRequest, string, bool> checkerDelegate)
        {
            try
            {
                var response = this.importController.PostSyncProfileRequest(request);

                if (response is System.Web.Http.Results.BadRequestErrorMessageResult)
                {
                    var errMsgResponse = (System.Web.Http.Results.BadRequestErrorMessageResult)response;
                    return checkerDelegate(request, errMsgResponse.Message);
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Assert.Fail("Unexpected error code: {0}", ex.Message);
                return false;
            }
        }
    }
}
