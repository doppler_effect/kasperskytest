﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using HostApp.DataModel;

namespace Tests
{
    public abstract class AbstractTestClass
    {
        protected abstract bool CheckUserImportForDataValidation(SyncProfileRequest request, Func<SyncProfileRequest, string, bool> checkerDelegate);

        protected string[] badLocales
        {
            get
            {
                return new string[] { "ru", "RU", "RUR", "RU-RU", "ru1-RU", "ru2-RU", "ru-ru", "RU-ru", null };
            }
        }

        protected string[] badCountryCodes
        {
            get
            {
                return new string[] { "RUR", "1RUR", "12", "A", "--", "--%--", "", "123", null };
            }
        }

        /// <summary>
        /// Creates a new unique request data object
        /// </summary>
        protected SyncProfileRequest GenerateNewRequest()
        {
            return new SyncProfileRequest()
            {
                AdvertisingOptIn = true,
                CountryIsoCode = "RU",
                DateModified = DateTime.Now,
                Locale = "ru-RU",
                RequestId = Guid.NewGuid(),
                UserId = Guid.NewGuid()
            };
        }

        /// <summary>
        /// Assures that data validation check for given Locale returns failure
        /// </summary>
        protected void CheckLocaleIsInvalid(string locale)
        {
            var request = GenerateNewRequest();
            request.Locale = locale;

            //checks that givan request in invalid with given reason
            bool checkResult = CheckUserImportForDataValidation(request, LocaleErrorDelegate);

            Assert.IsTrue(checkResult, "Data validation check didn't catch Locale '{0}'", request.Locale);
        }

        /// <summary>
        /// Assures that data validation check for given CountryIsoCode returns failure
        /// </summary>
        protected void CheckCountryCodeIsInvalid(string countryIsoCode)
        {
            var request = GenerateNewRequest();
            request.CountryIsoCode = countryIsoCode;

            bool checkResult = CheckUserImportForDataValidation(request, CountryCodeErrorDelegate);

            Assert.IsTrue(checkResult, "Data validation check didn't catch CountryCode '{0}'", request.CountryIsoCode);
        }
        
        /// <summary>
        /// Looks for expected error message
        /// </summary>
        protected bool CountryCodeErrorDelegate(SyncProfileRequest request, string errorMessage)
        {
            return errorMessage.Contains(String.Format("Contry code '{0}' invalid length", request.CountryIsoCode)) ||
                errorMessage.Contains(String.Format("Contry code '{0}' contains non-letter symbols", request.CountryIsoCode)) ||
                errorMessage.Contains(String.Format("Empty contry code"));
        }

        /// <summary>
        /// Looks for expected error message
        /// </summary>
        protected bool LocaleErrorDelegate(SyncProfileRequest request, string errorMessage)
        {
            return errorMessage.Contains(String.Format("Locale data '{0}' doesn't match ISO pattern", request.Locale)) ||
                errorMessage.Contains(String.Format("Empty locale"));
        }
    }
}
