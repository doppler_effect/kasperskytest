﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace HostApp
{
    internal static class Log
    {
        public static ILogger Logger;

        static Log()
        {
            Log.Logger = new LoggerConfiguration()
                //.ReadFrom.AppSettings()
                .MinimumLevel.Verbose()
                .WriteTo.ColoredConsole(/*restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information*/)
                .WriteTo.RollingFile(@"Log-{Date}.log")
                .CreateLogger();            
        }
    }
}
