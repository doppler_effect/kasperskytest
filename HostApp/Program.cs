﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostApp
{
    class Program
    {
        const string url = "http://localhost:8080";

        private static WCF.UserInfoServiceWrapper infoService;
        private static WebApi.UserImportServiceWrapper importService;

        static void Main(string[] args)
        {
            StartUserInfoService();
            StartUserImportService();

            Log.Logger.Warning("Press any key to stop services...");
            Console.ReadKey();

            StopServices();
        }

        /// <summary>
        /// Starts a hosted WebApi service
        /// </summary>
        static void StartUserImportService()
        {
            try
            {
                Log.Logger.Information("Starting User Import webservice...");
                importService = new WebApi.UserImportServiceWrapper(url);
            }
            catch (Exception ex)
            {
                Log.Logger.Error("Error starting User Import webservice: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Starts a hosted WCF service
        /// </summary>
        static void StartUserInfoService()
        {
            try
            {
                Log.Logger.Information("Starting User Information service...");
                infoService = new WCF.UserInfoServiceWrapper(url);     
            }
            catch (Exception ex)
            {
                Log.Logger.Error("Error starting User Information service: {0}", ex.Message);
            }
        }

        /// <summary>
        /// Initiate stopping of both services
        /// </summary>
        static void StopServices()
        {
            try
            {
                Log.Logger.Information("Stopping Info Service...");
                if (infoService != null)
                    infoService.Dispose();

                Log.Logger.Information("Stopping Import Service...");
                if (importService != null)
                    importService.Dispose();
            }
            catch (Exception ex)
            {
                Log.Logger.Error("Error stopping services: {0}", ex.Message);
            }
            Log.Logger.Information("Press any key to quit...");
            Console.ReadKey();
        }
    }
}
