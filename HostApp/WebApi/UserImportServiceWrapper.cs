﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web.Http;
using System.Web.Http.SelfHost;

namespace HostApp.WebApi
{
    /// <summary>
    /// A simple wrapper class that incapsulates configuration and startup of WebApi service
    /// </summary>
    public class UserImportServiceWrapper : IDisposable
    {
        private HttpSelfHostServer server;

        public UserImportServiceWrapper(string url)
        {
            try
            {
                var config = new HttpSelfHostConfiguration(url);

                config.Routes.MapHttpRoute(
                    name: "UserImportApi",
                    routeTemplate: "{controller}.json"
                );
                
                server = new HttpSelfHostServer(config);

                server.OpenAsync().Wait();

                Log.Logger.Information("User Import webservice successfully started.");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error starting WebApi host server");
                throw;
            }
        }

        public void Dispose()
        {
            server.CloseAsync().Wait();
            server.Dispose();

            Log.Logger.Information("User Import service stopped.");
        }
    }
}
