﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using HostApp.DataModel;

namespace HostApp.WebApi
{
    public class ImportController : ApiController
    {
        private DBWrapper db;
        
        /// <summary>
        /// default ctor, uses UserContext class underneath
        /// </summary>
        public ImportController()
        {
            this.db = new DBWrapper();
        }

        /// <summary>
        /// ctor for unit testing, uses provided mock UserContext object
        /// </summary>
        /// <param name="mockContext">fake IUserContext implementation for testing</param>
        public ImportController(IUserContext mockContext)
        {
            this.db = new DBWrapper(mockContext);
        }

        /// <summary>
        /// Action to trigger on POST request to the controller.
        /// </summary>
        /// <param name="syncProfileRequest">received JSON object</param>
        [HttpPost]
        public IHttpActionResult PostSyncProfileRequest(SyncProfileRequest syncProfileRequest)
        {
            try
            {
                Log.Logger.Debug("Received a User Import request with the following data: {@request}", syncProfileRequest);

                if (!ModelState.IsValid)
                {
                    Log.Logger.Error("Wrong data model state in request, returning BadRequest");
                    return BadRequest(ModelState);
                }

                string dataCheckDetails;
                if (!this.ValidateData(syncProfileRequest, out dataCheckDetails))
                {
                    Log.Logger.Error("Data validation failed for reason {reason}, returning BadRequest", dataCheckDetails);
                    return BadRequest(dataCheckDetails);
                }

                if (db.AddOrUpdateUser(syncProfileRequest))
                {
                    Log.Logger.Debug("User has been successfully processed, returning OK");
                    return Ok();
                }
                else
                {
                    var res = new System.Web.Http.Results.NotFoundResult(this);
                    Log.Logger.Debug("There was an error processing user into DB, returning NOOK");
                    return res;
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error during request processing");
                var res = new System.Web.Http.Results.ExceptionResult(ex, this);
                return res;
            }
        }
        
        /// <summary>
        /// Method for data validation
        /// </summary>
        /// <param name="data">JSON object</param>
        /// <param name="detailedMessage">String to put detailed result if checks fail</param>
        protected bool ValidateData(SyncProfileRequest data, out string detailedMessage)
        {
            detailedMessage = string.Empty;
            try
            {
                Log.Logger.Verbose("Starting data validation");
                
                //CountryIsoCode check
                if(String.IsNullOrWhiteSpace(data.CountryIsoCode))
                {
                    detailedMessage = String.Format("Empty contry code");
                    return false;
                }
                if (!data.CountryIsoCode.All(x => char.IsLetter(x)))
                {
                    detailedMessage = String.Format("Contry code '{0}' contains non-letter symbols", data.CountryIsoCode);
                    return false;
                }
                if (data.CountryIsoCode.Length != 2)
                {
                    detailedMessage = String.Format("Contry code '{0}' invalid length", data.CountryIsoCode);
                    return false;
                }

                //Locale check
                if (String.IsNullOrWhiteSpace(data.Locale))
                {
                    detailedMessage = String.Format("Empty locale");
                    return false;
                }
                string regExPattern = @"^[a-z]{2,}-[A-Z]{2,}$";
                var regEx = new System.Text.RegularExpressions.Regex(regExPattern);
                if(!regEx.IsMatch(data.Locale) )
                {
                    detailedMessage = String.Format("Locale data '{0}' doesn't match ISO pattern", data.Locale);
                    return false;
                }

                Log.Logger.Verbose("Data validation completed");
                return true;
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error during data validation");
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}