﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using HostApp.DataModel;

namespace HostApp.WCF
{
    public class UserInfoProvider : IUserInfoProvider
    {
        private DBWrapper db;

        /// <summary>
        /// default ctor, uses UserContext class underneath
        /// </summary>
        public UserInfoProvider()
        {
            this.db = new DBWrapper();
        }

        /// <summary>
        /// ctor for unit testing, uses provided mock UserContext object
        /// </summary>
        /// <param name="mockContext">fake IUserContext implementation for testing</param>
        public UserInfoProvider(IUserContext mockContext)
        {
            this.db = new DBWrapper(mockContext);
        }

        /// <summary>
        /// A service contract implementation
        /// </summary>
        /// <param name="userId">user unique ID</param>
        /// <returns>A UserInfo - typed object</returns>
        public UserInfo GetUserInfo(Guid userId)
        {
            try
            {
                Log.Logger.Debug("Received a User Info request with the following ID: {UserId}", userId);
                
                string requestResultDetails = null;
                var queryResult = db.GetUser(userId, ref requestResultDetails);

                if (queryResult == null)
                {
                    UserNotFound error = new UserNotFound()
                    {
                        UserId = userId,
                        ErrorDetails = requestResultDetails
                    };
                    Log.Logger.Debug("User not found, throwing a FaultException {ex}", error);
                    throw new FaultException<UserNotFound>(error, requestResultDetails);
                }
                else
                {
                    Log.Logger.Verbose("Found the user, creating response object");

                    UserInfo result = new UserInfo();
                    result.AdvertisingOptIn = queryResult.AdvertisingOptIn;
                    result.CountryIsoCode = queryResult.CountryIsoCode;
                    result.DateModified = queryResult.DateModified;
                    result.Locale = queryResult.Locale;
                    result.UserId = queryResult.UserId;

                    Log.Logger.Debug("Returning user info for request: {@User}", result);

                    return result;
                }
            }
            catch(FaultException)
            {
                throw;
            }
            catch (Exception ex)
            {
                UserNotFound error = new UserNotFound()
                {
                    UserId = userId,
                    ErrorDetails = ex.Message
                };
                
                Log.Logger.Error(ex, "Error during request processing");
                throw new FaultException<UserNotFound>(error, ex.Message);
            }
        }
    }
}
