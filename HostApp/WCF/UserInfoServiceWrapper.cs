﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace HostApp.WCF
{
    /// <summary>
    /// Wrapper class that incapsulates configuration and startup of WCF service
    /// </summary>
    public class UserInfoServiceWrapper : IDisposable
    {
        private ServiceHost infoService;

        public UserInfoServiceWrapper(string url)
        {
            try
            {
                Uri baseAddress = new Uri(string.Format("{0}/wcftest", url));

                infoService = new ServiceHost(typeof(WCF.UserInfoProvider), baseAddress);

                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                infoService.Description.Behaviors.Add(smb);

                infoService.Open();

                Log.Logger.Information("User Information service successfully started.");
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error starting WCF service");
                throw;
            }
        }

        public void Dispose()
        {
            this.infoService.Close();

            Log.Logger.Information("User Information service stopped.");
        }
    }
}
