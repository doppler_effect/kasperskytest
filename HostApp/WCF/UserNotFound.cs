﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace HostApp.WCF
{
    [DataContract]
    public class UserNotFound
    {
        [DataMember]
        public Guid UserId { get; set; }

        [DataMember]
        public string ErrorDetails { get; set; }
    }
}
