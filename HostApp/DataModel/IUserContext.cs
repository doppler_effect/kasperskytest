﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace HostApp.DataModel
{
    public interface IUserContext: IDisposable
    {
        DbSet<SyncProfileRequest> SyncProfileRequests { get; set; }
        int SaveChanges();
    }
}
