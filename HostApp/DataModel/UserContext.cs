﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HostApp.DataModel
{
    class UserContext : DbContext, IUserContext
    {
        public UserContext() : base("name=UserContext")
        {
        }

        public DbSet<SyncProfileRequest> SyncProfileRequests { get; set; }
    }
}
