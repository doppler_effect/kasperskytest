﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HostApp.DataModel
{
    public abstract class MyAccountRequestBase
    {
        [Key]
        public Guid UserId { get; set; }
        public Guid RequestId { get; set; }
    }
}