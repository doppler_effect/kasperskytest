﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HostApp.DataModel
{
    public class DBWrapper : IDisposable
    {
        private IUserContext db;

        /// <summary>
        /// default ctor, creates instance of UserContext for normal operation.
        /// </summary>
        public DBWrapper()
        {
            Log.Logger.Verbose("Creating DBWrapper with a new instance of UserContext");
            this.db = new UserContext();
        }

        /// <summary>
        /// ctor, which takes a mocked UserContext object - for future unit testing
        /// </summary>
        /// <param name="context"></param>
        public DBWrapper(IUserContext context)
        {
            Log.Logger.Verbose("Creating DBWrapper with an instance of IUserContext - {0}", context.GetType());
            this.db = context;
        }

        /// <summary>
        /// Searches for user with given ID in database
        /// </summary>
        /// <param name="UserID">User's unique ID</param>
        /// <param name="details">a string for details in case of error</param>
        public virtual SyncProfileRequest GetUser(Guid UserID, ref string details)
        {
            try
            {
                Log.Logger.Verbose("Starting a GetUser database method");

                var result = db.SyncProfileRequests.Where(s => s.UserId == UserID).FirstOrDefault();
                if (result == null)
                {
                    details = "User not found";
                    Log.Logger.Verbose("No user with id {id} found in DB", UserID);

                    return null;
                }
                else
                {
                    Log.Logger.Verbose("User found!");
                    return result;
                }
            }
            catch (Exception ex)
            {
                details = "Error accessing the database";
                Log.Logger.Error(ex, details);
                return null;
            }
        }

        /// <summary>
        /// Checks if user with such ID exists in DB, and then updates or adds it
        /// </summary>
        public virtual bool AddOrUpdateUser(SyncProfileRequest syncProfileRequest)
        {
            try
            {
                Log.Logger.Verbose("Starting a AddOrUpdateUser database method");

                if (ProfileExists(syncProfileRequest))
                {
                    Log.Logger.Verbose("User exists. Updating.");

                    var existingData = db.SyncProfileRequests.Where(data => data.UserId == syncProfileRequest.UserId).First();

                    existingData.RequestId = syncProfileRequest.RequestId;
                    existingData.CountryIsoCode = syncProfileRequest.CountryIsoCode;
                    existingData.Locale = syncProfileRequest.Locale;
                    existingData.DateModified = syncProfileRequest.DateModified;
                    existingData.AdvertisingOptIn = syncProfileRequest.AdvertisingOptIn;

                }
                else
                {
                    Log.Logger.Verbose("No such user in database. Creating one.");
                    db.SyncProfileRequests.Add(syncProfileRequest);
                }

                db.SaveChanges();
                Log.Logger.Verbose("Operation succeeded!");

                return true;
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error accessing the database.");
                return false;
            }
        }

        public virtual void Dispose()
        {
            if(db != null)
                db.Dispose();
        }

        protected virtual bool ProfileExists(SyncProfileRequest request)
        {
            Log.Logger.Verbose("Checking if user with id {id} exists in database", request.UserId);
            return db.SyncProfileRequests.Where(e => e.UserId == request.UserId).Any();
        }
    }
}
